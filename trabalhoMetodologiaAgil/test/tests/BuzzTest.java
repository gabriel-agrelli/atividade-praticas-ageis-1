/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import trabalhometodologiaagil.Buzz;

/**
 *
 * @author gabrielagrelli
 */
public class BuzzTest {
   
    @Test
    public void testBuzz() {
        assertEquals(Buzz.calculate(10), "Buzz");
        assertEquals(Buzz.calculate(5), "Buzz");
        assertEquals(Buzz.calculate(3), 3);
        assertEquals(Buzz.calculate(1), 1);
    }
}
