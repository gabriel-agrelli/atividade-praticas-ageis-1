/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhometodologiaagil;

/**
 *
 * @author gabrielagrelli
 */

public class Jokenpo {
    
    public enum item {
        pedra, papel, tesoura
    }
    
    public static String resultado(item jogada1, item jogada2) {
        if(jogada1 == jogada2) {
            return "Empate";
        } else if (jogada1 == item.pedra) {
            if(jogada2 == item.papel){
                return "Jogador 2";
            } else {
                return "Jogador 1";
            }
        } else if(jogada1 == item.papel) {
            if(jogada2 == item.pedra) {
                return "Jogador 1";
            } else {
                return "Jogador 2";
            }
        } else {
            if(jogada2 == item.pedra) {
                return "Jogador 2";
            } else {
                return "Jogador 1";
            }
        }
    } 
}
