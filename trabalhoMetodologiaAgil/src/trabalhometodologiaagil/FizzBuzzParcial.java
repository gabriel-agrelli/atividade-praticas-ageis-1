/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhometodologiaagil;

/**
 *
 * @author gabrielagrelli
 */
public class FizzBuzzParcial {
    
    public static String calculate(int number) {
        if(number % 15 == 0) {
            return "FizzBuzz";
        } else {
            return String.valueOf(number);
        }
    }
}
