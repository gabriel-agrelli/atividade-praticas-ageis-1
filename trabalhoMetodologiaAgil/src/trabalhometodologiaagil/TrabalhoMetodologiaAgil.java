/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhometodologiaagil;

import java.util.Scanner;

/**
 *
 * @author gabrielagrelli
 */
public class TrabalhoMetodologiaAgil {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        System.out.println("Teste do Fizz");
        System.out.println(Fizz.calculate(10).equals("10"));
        System.out.println(Fizz.calculate(3).equals("Fizz"));
        System.out.println(Fizz.calculate(6).equals("Fizz"));
        System.out.println(Fizz.calculate(14).equals("14"));
        
        System.out.println("Teste do Buzz");
        System.out.println(Buzz.calculate(5).equals("Buzz"));
        System.out.println(Buzz.calculate(7).equals("7"));
        System.out.println(Buzz.calculate(10).equals("Buzz"));
        System.out.println(Buzz.calculate(12).equals("12"));
        
        System.out.println("Teste do Fizbuzz parcial");
        System.out.println(FizzBuzzParcial.calculate(15).equals("Fizzbuzz"));
        System.out.println(FizzBuzzParcial.calculate(30).equals("Fizzbuzz"));
        System.out.println(FizzBuzzParcial.calculate(3).equals("3"));
        System.out.println(FizzBuzzParcial.calculate(7).equals("7"));
        
        System.out.println("Teste do Fizbuzz");
        System.out.println(FizzBuzzParcial.calculate(15).equals("Fizzbuzz"));
        System.out.println(FizzBuzzParcial.calculate(30).equals("Fizzbuzz"));
        System.out.println(FizzBuzzParcial.calculate(25).equals("Buzz"));
        System.out.println(FizzBuzzParcial.calculate(10).equals("Buzz"));
        System.out.println(FizzBuzzParcial.calculate(9).equals("Fizz"));
        System.out.println(FizzBuzzParcial.calculate(6).equals("FIzz"));
        System.out.println(FizzBuzzParcial.calculate(8).equals("8"));
        System.out.println(FizzBuzzParcial.calculate(1).equals("1"));

        System.out.println("Teste do JokenpoRefatorado");
        System.out.println(JokenpoRefatorado.resultado(JokenpoRefatorado.item.pedra, JokenpoRefatorado.item.pedra).equals("Empate"));
        System.out.println(JokenpoRefatorado.resultado(JokenpoRefatorado.item.papel, JokenpoRefatorado.item.pedra).equals("Jogador 1"));
        System.out.println(JokenpoRefatorado.resultado(JokenpoRefatorado.item.tesoura, JokenpoRefatorado.item.pedra).equals("Jogador 2"));
        
        System.out.println(JokenpoRefatorado.resultado(JokenpoRefatorado.item.pedra, JokenpoRefatorado.item.papel).equals("Jogador 2"));
        System.out.println(JokenpoRefatorado.resultado(JokenpoRefatorado.item.papel, JokenpoRefatorado.item.papel).equals("Empate"));
        System.out.println(JokenpoRefatorado.resultado(JokenpoRefatorado.item.tesoura, JokenpoRefatorado.item.papel).equals("Jogador 1"));

        
        System.out.println(JokenpoRefatorado.resultado(JokenpoRefatorado.item.pedra, JokenpoRefatorado.item.tesoura).equals("Jogador 1"));
        System.out.println(JokenpoRefatorado.resultado(JokenpoRefatorado.item.papel, JokenpoRefatorado.item.tesoura).equals("Jogador 2"));
        System.out.println(JokenpoRefatorado.resultado(JokenpoRefatorado.item.tesoura, JokenpoRefatorado.item.tesoura).equals("Empate"));
    }
}
