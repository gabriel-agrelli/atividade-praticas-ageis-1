/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhometodologiaagil;

/**
 *
 * @author gabrielagrelli
 */
public class Fizz {

    public static String calculate(int number) {
        if (number % 3 == 0) {
            return "Fizz";
        } else {
            return String.valueOf(number);
        }
    }
};
